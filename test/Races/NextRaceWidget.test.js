import { mount } from '@vue/test-utils'
import NextRaceWidget from '../../src/components/Races/components/NextRaceWidget.vue'
import { createStore } from 'vuex'
import { key } from '@/store'
import { describe, it } from 'vitest'

//Import real store but could be a test store
import races from '../../src/store/modules/races'
import currencies from '../../src/store/modules/currencies'
import state from '../../src/store/sharedState'
import getters from '../../src/store/sharedGetters'
import actions from '../../src/store/sharedActions'
import mutations from '../../src/store/sharedMutations'

const store = createStore({
    state,
    getters,
    actions,
    mutations,
    modules: {
        races,
        currencies
    },
});

const race = {
    "id_race": 1647215,
    "event": {
      "title": "Redcliffe",
      "country": "IE"
    },
    "race_type": "T",
    "post_time": 1439970900,
    "num_runners": 9,
    "distance": 1780,
    "purse": {
      "amount": 250,
      "currency": "GBP"
    },
    "runners": [
      {
        "id_runner": 15717421,
        "name": "Triumphant Knight",
        "odds": 4.7,
        "silk": ""
      },
      {
        "id_runner": 15717423,
        "name": "My Aliyana",
        "odds": 3,
        "silk": ""
      },
      {
        "id_runner": 15717425,
        "name": "Badjellys Courage",
        "odds": 4,
        "silk": ""
      }
    ]
  };

describe('NextRaceWidget Test', () => {
    it('Component must emit filter event when an input is clicked', () => {

        const wrapper = mount(NextRaceWidget, {
            props: {
                race: race
            },
            global: {
                plugins: [[store, key]],
            }
        })

        expect(wrapper.find('.runner-odds').isVisible()).toBe(true)

        expect(wrapper.findAll('button').length).toBe(race.runners.length)
        
    })
})