import { mount } from '@vue/test-utils'
import RaceTypeFilter from '../../src/components/Races/components/RaceTypeFilter.vue'
import { createStore } from 'vuex'
import { key } from '@/store'
import { describe, it } from 'vitest'

//Import real store but could be a test store
import races from '../../src/store/modules/races'
import currencies from '../../src/store/modules/currencies'
import state from '../../src/store/sharedState'
import getters from '../../src/store/sharedGetters'
import actions from '../../src/store/sharedActions'
import mutations from '../../src/store/sharedMutations'

const store = createStore({
  state,
  getters,
  actions,
  mutations,
  modules: {
    races,
    currencies
  },
})

describe('RaceTypeFilter Test', () => {
  it('Component must emit filter event when an input is clicked', () => {
    const wrapper = mount(RaceTypeFilter, {
      global: {
        plugins: [[store, key]],
    }
    })

    wrapper.find('input').trigger('click')

    expect(wrapper.emitted().filter.length).toBe(1)
  })
})