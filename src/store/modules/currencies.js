import data from '../../api/data'

const state = () => ({
    currencies: [
        {
            currency: 'EUR',
            value: 1,
        },
        {
            currency: 'GBP',
            value: 1.18,
        }
    ]
})

// getters
const getters = {
    currencies: state => {
        return state.currencies;
    }
}

export default {
    namespaced: true,
    state,
    getters,
}