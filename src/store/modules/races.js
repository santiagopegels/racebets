import data from '../../api/data'

const state = () => ({
  races: [],
  raceStatus: '',
  raceFilters: [{
    title: 'Gallop',
    value: 'G',
    default: true,
    icon: 'race-type-G.svg'
  },
  {
    title: 'Jumping',
    value: 'J',
    default: true,
    icon: 'race-type-J.svg'
  },
  {
    title: 'Trot',
    value: 'T',
    default: true,
    icon: 'race-type-T.svg'
  },
  {
    title: 'Dogs',
    value: 'D',
    default: false,
    icon: 'race-type-D.svg'
  }]
})

// getters
const getters = {
  races: state => {
    return state.races;
  },
  raceStatus: state => {
    return state.raceStatus;
  },
  raceFilters: state => {
    return state.raceFilters;
  }

}

// actions
const actions = {
  async fetchRaces({ commit, state }) {

    commit('TOGGLE_LOADING', '', {root:true})
    commit('SET_RACES', [])
    try {

      const response = await data.getRaces()
      commit('SET_RACES_RESPONSE', response.status)
      commit('SET_RACES', response.data)          

    } catch (e) {

      console.error(e)
      commit('SET_RACES_RESPONSE', 'failed')
    } finally {
      commit('TOGGLE_LOADING', '', {root:true})
    }
  },
}

// mutations
const mutations = {
  SET_RACES(state, { races }) {
    state.races = races
  },

  SET_RACES_RESPONSE(state, status ) {   
    state.raceStatus = status
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}