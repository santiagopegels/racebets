let mutations = {
    TOGGLE_LOADING(state, { races }) {
        state.isLoading = !state.isLoading
    },
}

export default mutations;