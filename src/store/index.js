import { createStore, createLogger } from 'vuex'
import races from './modules/races'
import currencies from './modules/currencies'
import state from './sharedState'
import getters from './sharedGetters'
import actions from './sharedActions'
import mutations from './sharedMutations'

const debug = process.env.NODE_ENV !== 'production'

export default createStore({
  state,
  getters,
  actions,
  mutations,
  modules: {
    races,
    currencies
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})