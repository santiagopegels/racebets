import dayjs from 'dayjs';

export function useTimeCalculator() {

    function substractWithNowTime(dateToSubstract) {
        const now = dayjs()
        return dateToSubstract.subtract(now)
    }

    return { substractWithNowTime };
}