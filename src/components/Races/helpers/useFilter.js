import { ref } from "vue";

export function useFilter() {
    const filters = ref([]);

    function setFilters(newFilters) {
        filters.value = newFilters.value;
    }

    function filterByRaceType(races) {
        if (filters.value.length) {
            return races.filter(({ race_type }) => {
                return filters.value.includes(race_type)
            });
        }

        return filters.value.length ? races : [];
    }

    return { filterByRaceType, setFilters };
}