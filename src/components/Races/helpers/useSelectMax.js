import { computed } from "vue";
import { useStore } from "vuex";

export function useSelectMax() {
    const store = useStore();

    const currencies = computed(() => store.getters["currencies/currencies"]);

    function selectMaxPurse(races) {

        if (!races.length) {
            return races;
        }

        return races.reduce(function (prev, current) {

            const prevAmount = calculatePurseAmountInEuro(prev.purse)
            const currentAmount = calculatePurseAmountInEuro(current.purse)

            return (prevAmount > currentAmount) ? prev : current
        })

    }

    function calculatePurseAmountInEuro(purse) {
        const currencyValueToConvertToEuro = currencies.value.find(({ currency }) => currency === purse.currency).value
        return purse.amount * currencyValueToConvertToEuro
    }

    return { selectMaxPurse };
}